<?php 
	function read($name){
	  $data = array();
	  foreach(file($name, FILE_IGNORE_NEW_LINES) as $value){
	  	$data[] = str_getcsv($value);
	  }
	  return $data;
	}

	function write($name, $data){
		$file = fopen($name, 'w');
		foreach ($data as $value) {
			fputcsv($file, $value);
		}
		fclose($file);
	}

	function insertNewData($contract_data, $award_data){
		foreach ($contract_data as $val1) {
		 	$test_flag = 0;
		 	foreach ($award_data as $val2) {
		 		if($val1[0] == $val2[0]){ 
		 			$new_items = array();
		 			$new_items[0] = $val1[0];
		 			$new_items[1] = $val1[1];
		 			$new_items[2] = $val1[2];
		 			$new_items[3] = $val1[3];
		 			$new_items[4] = $val1[4];
		 			$new_items[5] = $val1[5];
		 			$new_items[6] = $val1[6];
		 			$new_items[7] = $val1[7];
		 			$new_items[8] = $val2[1];
		 			$new_items[9] = $val2[2];
		 			$new_items[10] = $val2[3];
		 			$new_items[11] = $val2[4];
		 			$new_items[12] = $val2[5];
		 			$new_data_array[] = $new_items;
		 		}else{
		 			if($test_flag >= 1){
		 				$test_flag = $test_flag+1;
		 			}
		 			else{
		 				$test_flag = $test_flag+1;
		 			}
		 		}
		 	}
		 	if($test_flag != 3){
		 		$new_items = array();
		 		$new_items[0] = $val1[0];
		 		$new_items[1] = $val1[1];
		 		$new_items[2] = $val1[2];
		 		$new_items[3] = $val1[3];
		 		$new_items[4] = $val1[4];
		 		$new_items[5] = $val1[5];
		 		$new_items[6] = $val1[6];
		 		$new_items[7] = $val1[7];
		 		$new_items[8] = '';
		 		$new_items[9] = '';
		 		$new_items[10] = '';
		 		$new_items[11] = '';
		 		$new_items[12] = '';
		 		$new_data_array[] = $new_items;
		 	}
	 	}
	 	return $new_data_array;
	}

	function write_finalcsv($new_data_array){
		write('final.csv', $new_data_array);
		$new_amount = 0;
		foreach ($new_data_array as $val) {
			if($val[1] == 'Current' && $val[12] != ''){ 
				$new_amount = $val[12]+$new_amount;
			}
		}
		echo 'Total Amount of current contracts: '.$new_amount;
	}
 ?>