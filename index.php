<?php 
	include('includes/functions.php');
	header('Content-type: text/plain');
	$award = __DIR__.'\includes\awards.csv'; 
	$contract = __DIR__.'\includes\contracts.csv';
	$award_data = read($award); 
	unset($award_data[0][0]);
	$awardFlag = $award_data;
	unset($award_data[0]); 
	$contract_data = read($contract);
	$contractFlag = $contract_data;
  unset($contract_data[0]); 
	$first = $contractFlag[0]; 
	$second = $awardFlag[0]; 
	$heading = array_unique(array_merge($first,$second)); 
	$new_data_array = array(); 
  $new_data_array[] = $heading; 
	$new_data_array = insertNewData($contract_data, $award_data);
	write_finalcsv($new_data_array);
	
 ?>